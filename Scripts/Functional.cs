﻿using UnityEngine;
using UnityEngine.UI;

public class Functional : MonoBehaviour
{

    public void SimpleElemShaker(GameObject[] _mass)    //Простое визуальное перемешиваиние элементов массива
    {
        for (int _idElem = 0; _idElem < _mass.Length; _idElem++)
        {
            int rnd = Random.Range(0, _mass.Length);
            Vector3 _elemPos = _mass[rnd].transform.position;
            _mass[rnd].transform.position = _mass[_idElem].transform.position;
            _mass[_idElem].transform.position = _elemPos;           
        }
    }

    public void ElemShaker(GameObject[] _currMass, GameObject[] _nextMass)  //Перемешивание элементов при переходе на следующий этап
    {
        for (int _idElem = 0; _idElem < _currMass.Length; _idElem++)
        {
            _currMass[_idElem].SetActive(false);
        }
        for (int _idElem = 0; _idElem < _nextMass.Length; _idElem++)
        {
            int rnd = Random.Range(0, _nextMass.Length);
            Vector3 _elemPos = _nextMass[rnd].transform.position;
            _nextMass[rnd].transform.position = _nextMass[_idElem].transform.position;
            _nextMass[_idElem].transform.position = _elemPos;
        }
        for (int _idElem = 0; _idElem < _nextMass.Length; _idElem++)
        {
            _nextMass[_idElem].SetActive(true);
            _nextMass[_idElem].GetComponent<Image>().color = Color.white;
        }
    }

    public void ChangeElemColor(GameObject[] _mass, int _currElemId, string[] _rightMass)   //Перекрашивание кнопок в зависимости от нажатия на них (одежда, предметы)
    {
        for (int _massElemId = 0; _massElemId < _mass.Length; _massElemId++)
        {
            if (_massElemId == _currElemId)
            {
                if (_mass[_massElemId].GetComponent<Image>().color == Color.white)
                {
                    for (int _massRightElemId = 0; _massRightElemId < _rightMass.Length; _massRightElemId++)
                    {
                        if (_mass[_massElemId].GetComponentInChildren<Text>().text == _rightMass[_massRightElemId])
                        {
                            _mass[_massElemId].GetComponent<Image>().color = Color.green;
                            break;
                        }
                        else
                        {
                            _mass[_massElemId].GetComponent<Image>().color = Color.red;
                        }
                    }
                }
                else
                {
                    _mass[_massElemId].GetComponent<Image>().color = Color.white;
                }
            }
        }
    }

    public void ChangeElemColorKeys(GameObject[] _mass, int _currElemId, GameObject[] _keysMass)    //Перекрашивание кнопок в зависимости от нажатия на них (ключи)
    {
        for (int _massElemId = 0; _massElemId < _mass.Length; _massElemId++)
        {
            if (_massElemId == _currElemId)
            {
                if (_mass[_massElemId].GetComponent<Image>().color == Color.white)
                {
                    Debug.Log(_massElemId);
                    for (int _massKeyElemId = 0; _massKeyElemId < _keysMass.Length; _massKeyElemId++)
                    {
                        Debug.Log(_keysMass.Length);
                        if (_keysMass[_massKeyElemId].activeSelf == true && _massKeyElemId == _currElemId)
                        {
                            Debug.Log("Найден подходящий элемент");
                            _mass[_massElemId].GetComponent<Image>().color = Color.green;
                            break;
                        }
                        else
                        {
                            Debug.Log("Нет подходящего элемента");
                            _mass[_massElemId].GetComponent<Image>().color = Color.red;
                        }
                    }
                }
                else
                {
                    _mass[_massElemId].GetComponent<Image>().color = Color.white;
                }
            }
        }
    }

    public void HideAndShowElements(GameObject[] _previousMass, GameObject[] _nextMass) //Убрать элементы предыдущего этапа и отображение элементов текущего этапа
    {
        for (int _elemId = 0; _elemId < _previousMass.Length; _elemId++)
        {
            _previousMass[_elemId].SetActive(false);
        }
        for (int _elemId = 0; _elemId < _nextMass.Length; _elemId++)
        {
            _nextMass[_elemId].SetActive(true);
        }
    }

    public void ButtonsChange(GameObject[] _buttonsArray, int _id1, int _id2)   //Замена кнопок
    {
        _buttonsArray[_id1].SetActive(false);
        _buttonsArray[_id2].SetActive(true);
    }
}