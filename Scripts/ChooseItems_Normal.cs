﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChooseItems_Normal : Functional
{
    public GameObject[] buttons_upwear;
    public GameObject[] buttons_hats_shoes;
    public GameObject[] buttons_next;
    public GameObject[] buttons_prev;
    public GameObject[] buttons_items;
    public GameObject[] buttons_money;
    public GameObject[] buttons_keys;
    public GameObject[] images_locks;
    public GameObject[] text_instr;
    public GameObject[] btn_instr_next;
    public GameObject[] btn_instr_prev;
    public GameObject[] btn_intro;
    public GameObject[] text_intro;
    public GameObject[] references;
    public GameObject Intro_map;
    public GameObject Button_Hint;
    public GameObject Money_Info;
    public GameObject Btn_sound;
    public GameObject Btn_To_Draw_Way;
    public Text Instructions;
    public Text Money_value;    
    public int money = 0;
    private int id;
    private bool hint_flag = false;
    public GameObject Main_camera;
    public GameObject Instruction_Camera;
    public AudioSource sound;

    void Start()
    {
        Instructions.text = "Верхняя одежда";
        SimpleElemShaker(buttons_upwear);
    }

    public void ChangeButtonsColor_Upwear(int _id)
    {
        ChangeElemColor(buttons_upwear, _id, ChooseScene.clother);
    }

    public void ChangeButtonsColor_Hats_Shoes(int _id)
    {
        ChangeElemColor(buttons_hats_shoes, _id, ChooseScene.clother);
    }

    public void ChangeButtonsColor_Items(int _id)
    {
        ChangeElemColor(buttons_items, _id, ChooseScene.items);
    }

    public void ChangeButtonsColor_Money(int _id)
    {
        for (int i = 0; i < buttons_money.Length; i++)
        {
            if (i == _id)
            {
                if (buttons_money[i].GetComponent<Image>().color == Color.white)
                {
                    buttons_money[i].GetComponent<Image>().color = Color.green;
                }
                else
                {
                    buttons_money[i].GetComponent<Image>().color = Color.white;
                }
            }
        }
        id = _id;
    }

    public void ChangeButtonsColor_Keys(int _id)
    {
        ChangeElemColorKeys(buttons_keys, _id, images_locks);
    }

    public void Stage_1_Start()
    {
        Instruction_Camera.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_2_Start()
    {
        Instruction_Camera.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_3_Start()
    {
        Instruction_Camera.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_4_Start()
    {
        Instruction_Camera.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_1_Back()
    {
        SceneManager.LoadScene(0);
    }

    public void Stage_2_Back()
    {
        Instruction_Camera.SetActive(false);
        HideAndShowElements(buttons_items, buttons_hats_shoes);
        Instructions.text = "Головные уборы, аксессуары и обувь";
        ButtonsChange(buttons_prev, 2, 1);
        ButtonsChange(buttons_next, 2, 1);
        buttons_next[1].SetActive(true);
        Main_camera.SetActive(true);
    }

    public void Stage_3_Back()
    {
        Instruction_Camera.SetActive(false);
        HideAndShowElements(buttons_money, buttons_items);
        Instructions.text = "Предметы";
        ButtonsChange(buttons_prev, 3, 2);
        ButtonsChange(buttons_next, 3, 2);
        Money_value.text = "";
        Money_Info.SetActive(false);
        Button_Hint.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_4_Back()
    {
        Instruction_Camera.SetActive(false);
        HideAndShowElements(buttons_keys, buttons_money);
        Instructions.text = "Вам необходимо набрать сумму 1300 руб.";
        for (int i = 0; i < images_locks.Length; i++)
        {
            images_locks[i].SetActive(false);
        }
        ButtonsChange(buttons_prev, 4, 3);
        buttons_next[3].SetActive(true);
        Button_Hint.SetActive(true);
        hint_flag = false;
        Money_Info.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void ToStage2()
    {
        ElemShaker(buttons_hats_shoes, buttons_items);
        Instructions.text = "Предметы";
        ButtonsChange(buttons_prev, 1, 2);
        ButtonsChange(buttons_next, 1, 2);
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 0, 1);
        ButtonsChange(btn_instr_next, 0, 1);
        ButtonsChange(btn_instr_prev, 0, 1);
        Instruction_Camera.SetActive(true);
    }

    public void ToStage3()
    {
        ElemShaker(buttons_items, buttons_money);
        Instructions.text = "Вам необходимо набрать сумму 1300 руб.";
        ButtonsChange(buttons_prev, 2, 3);
        ButtonsChange(buttons_next, 2, 3);
        Button_Hint.SetActive(true);
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 1, 2);
        ButtonsChange(btn_instr_next, 1, 2);
        ButtonsChange(btn_instr_prev, 1, 2);
        Instruction_Camera.SetActive(true);
    }

    public void ToStage4()
    {
        ElemShaker(buttons_money, buttons_keys);
        Money_Info.SetActive(false);
        Button_Hint.SetActive(false);
        Instructions.text = "Нужный ключ";
        ButtonsChange(buttons_prev, 3, 4);
        ButtonsChange(buttons_next, 3, 4);
        int rnd = Random.Range(0, buttons_keys.Length);
        for (int i = 0; i < images_locks.Length; i++)
        {
            if (i != rnd)
            {
                images_locks[i].SetActive(false);
            }
            else
            {
                images_locks[i].SetActive(true);
            }
        }
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 2, 3);
        ButtonsChange(btn_instr_next, 2, 3);
        ButtonsChange(btn_instr_prev, 2, 3);
        Instruction_Camera.SetActive(true);
    }

    public void ToIntro()
    {
        Main_camera.SetActive(false);
        btn_instr_next[3].SetActive(false);
        btn_instr_prev[3].SetActive(false);
        text_instr[3].SetActive(false);
        Intro_map.SetActive(true);
        references[0].SetActive(true);
        btn_intro[0].SetActive(true);
        text_intro[0].SetActive(true);
        Instruction_Camera.SetActive(true);
    }

    public void NextIntroText_1()
    {
        ButtonsChange(btn_intro, 0, 1);
        ButtonsChange(text_intro, 0, 1);
        ButtonsChange(references, 0, 1);
    }

    public void NextIntroText_2()
    {
        references[1].SetActive(false);
        btn_intro[1].SetActive(false);
        ButtonsChange(text_intro, 1, 2);
        Btn_sound.SetActive(true);
        Btn_To_Draw_Way.SetActive(true);
    }

    public void NextStage_1()
    {
        ElemShaker(buttons_upwear, buttons_hats_shoes);
        Instructions.text = "Головные уборы, аксессуары и обувь";
        ButtonsChange(buttons_next, 0, 1);
        ButtonsChange(buttons_prev, 0, 1);        
    }

    public void PrevStage_1()
    {
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 1, 0);
        ButtonsChange(btn_instr_next, 1, 0);
        ButtonsChange(btn_instr_prev, 1, 0);
        Instruction_Camera.SetActive(true);
    }

    public void PrevStage_2()
    {
        HideAndShowElements(buttons_hats_shoes, buttons_upwear);
        Instructions.text = "Верхняя одежда";
        ButtonsChange(buttons_next, 1, 0);
        ButtonsChange(buttons_prev, 1, 0);
    }

    public void PrevStage_3()
    {
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 2, 1);
        ButtonsChange(btn_instr_next, 2, 1);
        ButtonsChange(btn_instr_prev, 2, 1);
        Instruction_Camera.SetActive(true);
    }

    public void PrevStage_4()
    {
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 3, 2);
        ButtonsChange(btn_instr_next, 3, 2);
        ButtonsChange(btn_instr_prev, 3, 2);
        Instruction_Camera.SetActive(true);
    }

    public void PrevStage_5()
    {
        Main_camera.SetActive(false);
        Instruction_Camera.SetActive(true);
    }  

    public void ChangeMoneyValue(int _value)
    {
        if (buttons_money[id].GetComponent<Image>().color == Color.green)
        {
            money += _value;
            Money_value.text = "Сумма " + money;
        }
        else
        {
            money -= _value;
            Money_value.text = "Сумма " + money;
        }
        if (money > 1300)
        {
            Money_value.GetComponent<Text>().color = Color.red;
        }
        else
        {
            Money_value.GetComponent<Text>().color = Color.black;
        }
    }

    public void ShowHint()
    {
        if (hint_flag == false)
        {
            Money_Info.SetActive(true);
            hint_flag = true;
            Debug.Log("Flag = true");
        }
        else
        {
            Money_Info.SetActive(false);
            hint_flag = false;
            Debug.Log("Flag = false");
        }
    }

    public void ToDrawWay()
    {
        SceneManager.LoadScene(2);
    }

    public void PlaySound()
    {
        sound.Play();
    }
}