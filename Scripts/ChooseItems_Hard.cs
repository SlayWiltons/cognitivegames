﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChooseItems_Hard : Functional
{
    public GameObject[] buttons_upwear_hats_shoes;
    public GameObject[] buttons_next;
    public GameObject[] buttons_prev;
    public GameObject[] buttons_items;
    public GameObject[] buttons_actions;
    public GameObject[] buttons_keys;
    public GameObject[] images_locks_1;
    public GameObject[] images_locks_2;
    public GameObject[] text_instr;
    public GameObject[] btn_instr_next;
    public GameObject[] btn_instr_prev;
    public Text Instructions;
    public int rnd, rnd2;
    public GameObject Main_camera;
    public GameObject Instruction_Camera;

    void Start()
    {
        SimpleElemShaker(buttons_upwear_hats_shoes);
        Instructions.text = "Верхняя одежда, головные уборы, аксессуары и обувь";
    }

    public void ChangeButtonsColor_Upwear_Hats_Shoes(int _id)
    {
        ChangeElemColor(buttons_upwear_hats_shoes, _id, ChooseScene.clother);
    }

    public void ChangeButtonsColor_Items(int _id)
    {
        ChangeElemColor(buttons_items, _id, ChooseScene.items);
    }

    public void ChangeButtonsColor_Keys(int _id)
    {
        for (int i = 0; i < buttons_keys.Length; i++)
        {
            if (i == _id)
            {
                if (buttons_keys[i].GetComponent<Image>().color == Color.white)
                {
                    Debug.Log(i);
                    for (int _keyI = 0; _keyI < images_locks_1.Length; _keyI++)
                    {
                        Debug.Log(images_locks_1.Length);
                        if ((images_locks_1[_keyI].activeSelf == true || images_locks_2[_keyI].activeSelf == true) && _keyI == _id)
                        {
                            Debug.Log("Найден подходящий элемент");
                            buttons_keys[i].GetComponent<Image>().color = Color.green;
                            break;
                        }
                        else
                        {
                            Debug.Log("Нет подходящего элемента");
                            buttons_keys[i].GetComponent<Image>().color = Color.red;
                        }
                    }
                }
                else
                {
                    buttons_keys[i].GetComponent<Image>().color = Color.white;
                }
            }
        }
    }

    public void Stage_1_Start()
    {
        Instruction_Camera.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_2_Start()
    {
        Instruction_Camera.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_3_Start()
    {
        Instruction_Camera.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_1_Back()
    {
        SceneManager.LoadScene(0);
    }

    public void Stage_2_Back()
    {
        Instruction_Camera.SetActive(false);
        HideAndShowElements(buttons_items, buttons_upwear_hats_shoes);
        Instructions.text = "Верхняя одежда, головные уборы, аксессуары и обувь";
        ButtonsChange(buttons_prev, 1, 0);
        ButtonsChange(buttons_next, 1, 0);
        Main_camera.SetActive(true);
    }

    public void Stage_3_Back()
    {
        Instruction_Camera.SetActive(false);
        Instructions.text = "С одним из ваших предметов что-то не так";
        for (int i = 0; i < buttons_items.Length; i++)
        {
            if (buttons_items[i].GetComponent<Image>().color == Color.white)
            {
                buttons_items[i].SetActive(false);
            }
            else
            {
                buttons_items[i].SetActive(true);
            }
        }
        for (int i = 0; i < buttons_actions.Length; i++)
        {
            buttons_actions[i].SetActive(true);
        }
        for (int i = 0; i < images_locks_1.Length; i++)
        {
            images_locks_1[i].SetActive(false);
        }
        for (int i = 0; i < images_locks_2.Length; i++)
        {
            images_locks_2[i].SetActive(false);
        }
        for (int i = 0; i < buttons_keys.Length; i++)
        {
            buttons_keys[i].SetActive(false);
        }
        ButtonsChange(buttons_prev, 3, 2);
        Main_camera.SetActive(true);
    }

    public void ToStage2()
    {
        ElemShaker(buttons_upwear_hats_shoes, buttons_items);
        Instructions.text = "Предметы";
        ButtonsChange(buttons_prev, 0, 1);
        ButtonsChange(buttons_next, 0, 1);
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 0, 1);
        ButtonsChange(btn_instr_next, 0, 1);
        ButtonsChange(btn_instr_prev, 0, 1);
        Instruction_Camera.SetActive(true);
    }

    public void ChooseAction(int _id)   //ToStage3
    {
        if (buttons_actions[_id].GetComponentInChildren<Text>().text == "Зарядить телефон")
        {
            Debug.Log("Выбрано правильное действие");
        }
        else
        {
            Debug.Log("Выбрано неправильное действие");
        }
        ElemShaker(buttons_items, buttons_keys);
        for (int i = 0; i < buttons_actions.Length; i++)
        {
            buttons_actions[i].SetActive(false);
        }
        Instructions.text = "Нужный ключ";
        do
        {
            rnd = Random.Range(0, buttons_keys.Length);
            rnd2 = Random.Range(0, buttons_keys.Length);
        }
        while (rnd == rnd2);
        for (int i = 0; i < images_locks_1.Length; i++)
        {
            if (i != rnd)
            {
                images_locks_1[i].SetActive(false);
            }
            else
            {
                images_locks_1[i].SetActive(true);
            }
        }
        for (int i = 0; i < images_locks_2.Length; i++)
        {
            if (i != rnd2)
            {
                images_locks_2[i].SetActive(false);
            }
            else
            {
                images_locks_2[i].SetActive(true);
            }
        }
        ButtonsChange(buttons_prev, 2, 3);
        Debug.Log(rnd + " " + rnd2);
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 1, 2);
        ButtonsChange(btn_instr_next, 1, 2);
        ButtonsChange(btn_instr_prev, 1, 2);
        Instruction_Camera.SetActive(true);
    }

    public void NextStage_1()
    {
        for (int id = 0; id < buttons_items.Length; id++)
        {
            if (buttons_items[id].GetComponentInChildren<Text>().text == "Телефон" && buttons_items[id].GetComponent<Image>().color == Color.green)
            {                
                Debug.Log("Заголовок сменился на правильный");
                buttons_next[1].SetActive(false);
                ButtonsChange(buttons_prev, 1, 2);
                for (int i = 0; i < buttons_items.Length; i++)
                {
                    if (buttons_items[i].GetComponent<Image>().color == Color.white)
                    {
                        buttons_items[i].SetActive(false);
                    }
                }
                for (int i = 0; i < buttons_actions.Length; i++)
                {
                    buttons_actions[i].SetActive(true);
                }
                Instructions.text = "С одним из ваших предметов что-то не так";
                break;
            }
            else
            {
                Instructions.text = "Вы выбрали не все предметы";
                Debug.Log("Заголовок сменился на неправильный");
            }
        }
    }

    public void PrevStage_1()
    {
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 1, 0);
        ButtonsChange(btn_instr_next, 1, 0);
        ButtonsChange(btn_instr_prev, 1, 0);
        Instruction_Camera.SetActive(true);
    }

    public void PrevStage_2()
    {        
        Main_camera.SetActive(false);
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 2, 1);
        ButtonsChange(btn_instr_next, 2, 1);
        ButtonsChange(btn_instr_prev, 2, 1);
        Instruction_Camera.SetActive(true);
        Instruction_Camera.SetActive(true);
    }

    public void PrevStage_3()
    {
        HideAndShowElements(buttons_items, buttons_items);
        Instructions.text = "Предметы";
        for (int i = 0; i < buttons_actions.Length; i++)
        {
            buttons_actions[i].SetActive(false);
        }
        buttons_next[1].SetActive(true);
        ButtonsChange(buttons_prev, 2, 1);        
    }

    public void PrevStage_4()
    {
        Main_camera.SetActive(false);
        Instruction_Camera.SetActive(true);
    }    
}