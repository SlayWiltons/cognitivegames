﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChooseItems_Easy : Functional
{
    public GameObject[] buttons_upwear;
    public GameObject[] buttons_hats;
    public GameObject[] buttons_shoes;
    public GameObject[] buttons_next;
    public GameObject[] buttons_prev;
    public GameObject[] buttons_items;
    public GameObject[] buttons_keys;
    public GameObject[] images_locks;
    public GameObject[] text_instr;
    public GameObject[] btn_instr_next;
    public GameObject[] btn_instr_prev;
    public GameObject[] btn_intro;
    public GameObject[] text_intro;
    public GameObject Btn_sound;
    public GameObject Intro_map;
    public Text Instructions;
    public GameObject Main_camera;
    public GameObject Instruction_Camera;
    public AudioSource sound;
    
    void Start() 
    {
        Instructions.text = "Верхняя одежда";
        SimpleElemShaker(buttons_upwear);
    }

    public void ChangeButtonsColor_Upwear(int _id)
    {
        ChangeElemColor(buttons_upwear, _id, ChooseScene.clother);
    }

    public void ChangeButtonsColor_Hats(int _id)
    {
        ChangeElemColor(buttons_hats, _id, ChooseScene.clother);
    }

    public void ChangeButtonsColor_Shoes(int _id)
    {
        ChangeElemColor(buttons_shoes, _id, ChooseScene.clother);
    }

    public void ChangeButtonsColor_Items(int _id)
    {
        ChangeElemColor(buttons_items, _id, ChooseScene.items);
    }

    public void ChangeButtonsColor_Keys(int _id)
    {        
        ChangeElemColorKeys(buttons_keys, _id, images_locks);
    }

    public void Stage_1_Start()
    {
        Instruction_Camera.SetActive(false);        
        Main_camera.SetActive(true);
    }

    public void Stage_2_Start()
    {
        Instruction_Camera.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_3_Start()
    {
        Instruction_Camera.SetActive(false);
        Main_camera.SetActive(true);
    }

    public void Stage_1_Back()
    {
        SceneManager.LoadScene(0);
    }

    public void Stage_2_Back()
    {
        Instruction_Camera.SetActive(false);
        HideAndShowElements(buttons_items, buttons_shoes);
        Instructions.text = "Обувь";
        ButtonsChange(buttons_next, 3, 2);
        ButtonsChange(buttons_prev, 3, 2);
        Main_camera.SetActive(true);
    }

    public void Stage_3_Back()
    {
        Instruction_Camera.SetActive(false);
        HideAndShowElements(buttons_keys, buttons_items);
        Instructions.text = "Предметы";
        for (int i = 0; i < images_locks.Length; i++)
        {
            images_locks[i].SetActive(false);
        }
        ButtonsChange(buttons_prev, 4, 3);
        buttons_next[3].SetActive(true);
        Main_camera.SetActive(true);
    }

    public void ToStage2()
    {
        ElemShaker(buttons_shoes, buttons_items);
        Instructions.text = "Предметы";
        ButtonsChange(buttons_next, 2, 3);
        ButtonsChange(buttons_prev, 2, 3);
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 0, 1);
        ButtonsChange(btn_instr_next, 0, 1);
        ButtonsChange(btn_instr_prev, 0, 1);
        Instruction_Camera.SetActive(true);
    }

    public void ToStage3()
    {
        ElemShaker(buttons_items, buttons_keys);
        Instructions.text = "Нужный ключ";
        ButtonsChange(buttons_prev, 3, 4);
        ButtonsChange(buttons_next, 3, 4);
        buttons_next[3].SetActive(false);
        int rnd = Random.Range(0, buttons_keys.Length);
        for (int i = 0; i < images_locks.Length; i++)
        {
            if (i != rnd)
            {
                images_locks[i].SetActive(false);
            }
            else
            {
                images_locks[i].SetActive(true);
            }
        }
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 1, 2);
        ButtonsChange(btn_instr_next, 1, 2);
        ButtonsChange(btn_instr_prev, 1, 2);
        Instruction_Camera.SetActive(true);
    }    

    public void ToIntro()
    {
        Main_camera.SetActive(false);
        btn_instr_next[2].SetActive(false);
        btn_instr_prev[2].SetActive(false);
        text_instr[2].SetActive(false);
        Intro_map.SetActive(true);
        btn_intro[0].SetActive(true);
        text_intro[0].SetActive(true);
        Instruction_Camera.SetActive(true);
    }

    public void NextIntroText_1()
    {
        ButtonsChange(btn_intro, 0, 1);
        ButtonsChange(text_intro, 0, 1);
    }

    public void NextIntroText_2()
    {
        ButtonsChange(btn_intro, 1, 2);
        ButtonsChange(text_intro, 1, 2);
        Btn_sound.SetActive(true);
    }

    public void NextIntroText_3()
    {
        ButtonsChange(btn_intro, 2, 3);
        ButtonsChange(text_intro, 2, 3);
        Btn_sound.SetActive(false);
    }

    public void NextStage_1()
    {
        ElemShaker(buttons_upwear, buttons_hats);
        Instructions.text = "Головные уборы и аксессуары";
        ButtonsChange(buttons_next, 0, 1);
        ButtonsChange(buttons_prev, 0, 1);
    }

    public void NextStage_2()   
    {
        ElemShaker(buttons_hats, buttons_shoes);
        Instructions.text = "Обувь";
        ButtonsChange(buttons_next, 1, 2);
        ButtonsChange(buttons_prev, 1, 2);
    }

    public void PrevStage_1()
    {
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 1, 0);
        ButtonsChange(btn_instr_next, 1, 0);
        ButtonsChange(btn_instr_prev, 1, 0);
        Instruction_Camera.SetActive(true);
    }

    public void PrevStage_2()
    {
        HideAndShowElements(buttons_hats, buttons_upwear);
        Instructions.text = "Верхняя одежда";
        ButtonsChange(buttons_next, 1, 0);
        ButtonsChange(buttons_prev, 1, 0);
    }

    public void PrevStage_3()
    {
        HideAndShowElements(buttons_shoes, buttons_hats);
        Instructions.text = "Головные уборы и аксессуары";
        ButtonsChange(buttons_next, 2, 1);
        ButtonsChange(buttons_prev, 2, 1);
    }  

    public void PrevStage_4()
    {        
        Main_camera.SetActive(false);
        ButtonsChange(text_instr, 2, 1);
        ButtonsChange(btn_instr_next, 2, 1);
        ButtonsChange(btn_instr_prev, 2, 1);
        Instruction_Camera.SetActive(true);
    }    

    public void PrevStage_5()
    {        
        Main_camera.SetActive(false);
        Instruction_Camera.SetActive(true);
    }   
    
    public void PlaySound()
    {
        sound.Play();
    }
}