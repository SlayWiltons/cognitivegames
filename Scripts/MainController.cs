﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainController : Functional
{
    public GameObject canvas_difficult_easy;
    public GameObject canvas_difficult_normal;
    public GameObject canvas_difficult_hard;
    public GameObject controller_difficult_easy;
    public GameObject controller_difficult_normal;
    public GameObject controller_difficult_hard;
    public GameObject canvas_instr_easy;
    public GameObject canvas_instr_normal;
    public GameObject canvas_instr_hard;
    public static string[] clother_winter = { "Свитер", "Брюки", "Пуховик", "Вязанная шапка", "Шарф", "Перчатки", "Варежки", "Меховые сапоги", "Меховые ботинки" };
    public static string[] clother_summer = { "Футболка", "Шорты", "Солнцезащитные очки", "Кепка", "Босоножки", "Кроссовки", "Юбка" };
    public static string[] clother_autumn = { "Пальто", "Легкая куртка", "Брюки", "Рубашка", "Шарф", "Зонт", "Тонкая шапка", "Сапоги", "Ботинки" };
    public static string[] items_easy = { "Паспорт", "Конверт с письмом", "Телефон", "Ключи" };
    public static string[] items_normal = { "Паспорт", "Социальная карта", "Телефон", "Карта магазина продуктов", "Ключи", "Деньги", "Список продуктов" };
    public static string[] items_hard = { "Паспорт", "Медицинский полис", "Социальная карта", "Телефон", "Ключи", "Результаты анализов" };
    public Texture[] weather_easy;
    public Texture[] weather_normal;
    public Texture[] weather_hard;
    public RawImage main_image;
    public GameObject MainCamera;
    public GameObject InstrCamera;


    void Start ()
    {
        if (ChooseScene.ID == 1)
        {
            controller_difficult_normal.SetActive(false);
            canvas_difficult_normal.SetActive(false);
            controller_difficult_hard.SetActive(false);
            canvas_difficult_hard.SetActive(false);
            canvas_instr_easy.SetActive(true);
            canvas_instr_normal.SetActive(false);
            canvas_instr_hard.SetActive(false);
            MainCamera.SetActive(false);
            InstrCamera.SetActive(true);
            if (ChooseScene.ID_WEATHER == 1)
            {
                main_image.texture = weather_easy[0];
            }
            if (ChooseScene.ID_WEATHER == 2)
            {
                main_image.texture = weather_easy[1];
            }
            if (ChooseScene.ID_WEATHER == 3)
            {
                main_image.texture = weather_easy[2];
            }
        }
        if (ChooseScene.ID == 2)
        {
            controller_difficult_easy.SetActive(false);
            canvas_difficult_easy.SetActive(false);
            controller_difficult_hard.SetActive(false);
            canvas_difficult_hard.SetActive(false);
            canvas_instr_easy.SetActive(false);
            canvas_instr_normal.SetActive(true);
            canvas_instr_hard.SetActive(false);
            MainCamera.SetActive(false);
            InstrCamera.SetActive(true);
            if (ChooseScene.ID_WEATHER == 1)
            {
                main_image.texture = weather_normal[0];
            }
            if (ChooseScene.ID_WEATHER == 2)
            {
                main_image.texture = weather_normal[1];
            }
            if (ChooseScene.ID_WEATHER == 3)
            {
                main_image.texture = weather_normal[2];
            }
        }
        if (ChooseScene.ID == 3)
        {
            controller_difficult_easy.SetActive(false);
            canvas_difficult_easy.SetActive(false);
            controller_difficult_normal.SetActive(false);
            canvas_difficult_normal.SetActive(false);
            canvas_instr_easy.SetActive(false);
            canvas_instr_normal.SetActive(false);
            canvas_instr_hard.SetActive(true);
            MainCamera.SetActive(false);
            InstrCamera.SetActive(true);
            if (ChooseScene.ID_WEATHER == 1)
            {
                main_image.texture = weather_hard[0];
            }
            if (ChooseScene.ID_WEATHER == 2)
            {
                main_image.texture = weather_hard[1];
            }
            if (ChooseScene.ID_WEATHER == 3)
            {
                main_image.texture = weather_hard[2];
            }
        }
    }

    public void Exit()
    {
        Application.Quit();
    }
}