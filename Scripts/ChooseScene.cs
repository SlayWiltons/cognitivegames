﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChooseScene : Functional
{
    public static int ID;
    public static int ID_WEATHER;
    public Text question;
    public GameObject[] ButtonsDifficult;
    public GameObject[] ButtonsWeather;
    public static string[] items;
    public static string[] clother;
    public Texture[] weather_type;
    public static Texture weather;

    public void LoadDifficult_1()   //Почта
    {
        ID = 1;
        items = MainController.items_easy;
        ShowNextChoise();        
    }
    public void LoadDifficult_2()   //Магазин
    {
        ID = 2;
        items = MainController.items_normal;
        ShowNextChoise();
    }
    public void LoadDifficult_3()   //Поликлиника
    {
        ID = 3;
        items = MainController.items_hard;
        ShowNextChoise();
    }
    public void LoadWeather_1() //Осень
    {
        ID_WEATHER = 1;
        clother = MainController.clother_autumn;
        SceneManager.LoadScene(1);
    }
    public void LoadWeather_2() //Лето
    {
        ID_WEATHER = 2;
        clother = MainController.clother_summer;
        SceneManager.LoadScene(1);
    }
    public void LoadWeather_3() //Зима
    {
        ID_WEATHER = 3;
        clother = MainController.clother_winter;
        SceneManager.LoadScene(1);
    }

    void ShowNextChoise()
    {
        question.text = "Выберите погоду:";
        HideAndShowElements(ButtonsDifficult, ButtonsWeather);
    }

    public void Exit()
    {
        Application.Quit();
    }
}